import axios from 'axios';
import React from 'react';
import { useRef } from 'react';
import { useDispatch } from 'react-redux';
import { addCard, getCard } from '../features/cardSlice';

const Form = () => {
    const dispatch = useDispatch();
    const titleRef = useRef();
    const imageRef = useRef ();
    const formRef = useRef();

    const handleSubmit = (e) => {
        e.preventDefault();
        const data = {
            title: titleRef.current.value,
            url : imageRef.current.value,
        }
        axios.post('https://jsonplaceholder.typicode.com/photos',data)
        .then(() => {
            dispatch(addCard(data));
            dispatch(getCard())
            formRef.current.reset()
        }) 
    }
    return (
        <div className='d-flex flex-start m-5 '>
            <form className='shadow-lg p-3 rounded justify-content-center align-items-center d-flex' ref={formRef} style={{height: 100}} onSubmit={handleSubmit}>
                <div className='form-group d-flex justify-content-center align-items-center'>
                <input ref={imageRef} type="text" className='form-control m-2' defaultValue={"https://via.placeholder.com/600/92c952"} style={{height:20}} disabled/>
                <input ref={titleRef} type="text" className='form-control m-2' placeholder='title...' style={{height:20, width:110}}/>
                <button type='submit' className='btn btn-success' style={{height:40}}> Enregistrer </button>
                </div>
                
            </form>
        </div>
    );
}

export default Form;
