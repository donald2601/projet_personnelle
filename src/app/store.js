import { configureStore } from "@reduxjs/toolkit";
import cardsliceReducer from '../features/cardSlice'


export default configureStore({
    reducer : {
        cards : cardsliceReducer,
    }
})