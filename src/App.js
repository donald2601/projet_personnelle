import './App.css';
import Cards from './components/Cards';
import Form from './components/Form';

function App() {
  return (
    <div className="App">
      <Form/>
      <Cards/>
    </div>
  );
}

export default App;
