import { createSlice } from "@reduxjs/toolkit";

const cardSlice = createSlice ({
    name: "cards",
    initialState : [],
    reducers: {
        getCard: (state , {payload})=>{
            state = payload
            return state
        },

        addCard:(state, {payload})=> {

            const newCards = {
                id: Date.now(),
                url : payload.url,
                title : payload.title,
            }
            state.push(newCards)
            return state
            
    },
    removeCard:(state, {payload})=> {
        state = state.filter(items => items.id !== payload)
        return state
    }
}})
export const {addCard, removeCard, getCard} = cardSlice.actions;
export default cardSlice.reducer