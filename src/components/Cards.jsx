import axios from 'axios';
import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getCard } from '../features/cardSlice';


const Cards = () => {
    const selector= useSelector(state => state.cards)
    const dispatch = useDispatch();
    // const [data, setData] = useState([]);
    const [error, setError] = useState(null);
    useEffect(()=>{
        axios.get('https://jsonplaceholder.typicode.com/photos')
        .then(res => dispatch (getCard(res.data.slice(0,9))))
        .catch(err => setError(err.message));
        
    },[])
    return (
        <>
        <div className='d-flex flex-wrap justify-content-center'>
            { error ? <div className='container '> {error} </div> :
            selector.map(items => (
                <div key={items.id} class="card mb-5 m-5" style={{width: "18rem"}}>
                        <img src={items.url} class="card-img-top" alt="errors" />
                    <div className='card-text'> 
                    <p> {items.title} </p>
                     </div>
                </div>) )
            }
        </div>
        </>
    );
}

export default Cards;
